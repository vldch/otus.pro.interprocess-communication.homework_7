﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace interprocess_communication_consol
{
    public class ThreadPoolCalculater
    {
        private ManualResetEvent _doneEvent;
        public int[] Arr { get; }
        public long Result { get; set; }
        public int StartIndex { get; set; }
        public int Count { get; set; }

        public ThreadPoolCalculater(int[] arr, int startIndex, int count, ManualResetEvent doneEvent)
        {
            Arr = arr;
            StartIndex = startIndex;
            Count = count;
            _doneEvent = doneEvent;
        }

        public void ThreadPoolCallback(object threadContext)
        {
            Result = CalculateArr(Arr, StartIndex, Count);

            _doneEvent.Set();
        }

        public long CalculateArr(int[] array, int startIndex, int count)
        {
            long resCalc = 0;
            for (var i = startIndex; i < startIndex + count; i++)
                resCalc += array[i];

            return resCalc;

        }
    }
}
