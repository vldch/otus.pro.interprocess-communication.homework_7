﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace interprocess_communication_consol
{
    public class RandomizerArray:Random
    {
        public int[] CreateRandomArray(int min, int max,int lenght)
        {
            Random randNum = new Random();

            var result = Enumerable
                .Repeat(0, lenght)
                .Select(i => randNum.Next(min, max))
                .ToArray();

            return result;
        }

        public int[] CreateRandomArray(int length)
        {
            var rnd = new Random();

            var arr = new int[length];
            for (var i = 0; i < length; i++)
                arr[i] = rnd.Next(-1_000, 1_000);

            return arr;

        }
    }
}
