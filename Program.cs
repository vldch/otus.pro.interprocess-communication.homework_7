﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

namespace interprocess_communication_consol
{
    public class Program
    {
        delegate long Calculation(int[] arr);
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            var length = 100_000;

            var arr = new RandomizerArray().CreateRandomArray(-100000, 100000, length);

            Calculate("Последовательное вычисление", arr, Helper.ConsequentialCalculation);
            Calculate("Параллельное с помощью LINQ", arr, Helper.ParallelCalculationWithLinq);
            Calculate("Параллельное с помощью ThreadPool", arr, Helper.ParallelCalculationWithThreadPool);

            Console.WriteLine();

            length = 1_000_000;

            arr = new RandomizerArray().CreateRandomArray(-100000, 100000, length);

            Calculate("Последовательное вычисление", arr, Helper.ConsequentialCalculation);
            Calculate("Параллельное с помощью LINQ", arr, Helper.ParallelCalculationWithLinq);
            Calculate("Параллельное с помощью ThreadPool", arr, Helper.ParallelCalculationWithThreadPool);

            Console.WriteLine();

            length = 1000000000;

            arr = new RandomizerArray().CreateRandomArray(-100000, 100000, length);

            Calculate("Последовательное вычисление", arr, Helper.ConsequentialCalculation);
            Calculate("Параллельное с помощью LINQ", arr, Helper.ParallelCalculationWithLinq);
            Calculate("Параллельное с помощью ThreadPool", arr, Helper.ParallelCalculationWithThreadPool);

        }

        private static void Calculate(string name, int[] arr, Calculation calculation)
        {
            var stopWatch = new Stopwatch();

            stopWatch.Start();

            var sum = calculation.Invoke(arr);

            stopWatch.Stop();

            Console.WriteLine($"Результат: {sum}");

            Console.WriteLine($"{name}, Длина: {arr.Length:N0}, Время: {stopWatch.Elapsed}");
        }
    }
}
