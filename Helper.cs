﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace interprocess_communication_consol
{
    public class Helper
    {
        public static long ConsequentialCalculation(int[] arr)
        {
            long res = 0;

            foreach (var a in arr)
            {
                res += a;
            }

            return res;
        }

        public static long ParallelCalculationWithLinq(int[] arr)
        {
            var sum = arr.AsParallel().Sum();

            return sum;
        }

        public static long ParallelCalculationWithThreadPool(int[] arr)
        {
            long res = 0;

            const int threadsCount = 15;

            var chunkLength = arr.Length / threadsCount;

            var parts = Enumerable.Range(0, threadsCount)
                .Select(i => arr.Skip(i * chunkLength).Take(chunkLength))
                .ToArray();

            var doneEvents = new ManualResetEvent[parts.Count()];

            var resArrCalcs = new ThreadPoolCalculater[parts.Count()];

            var nextIndex = 0;

            var partsCount = parts.Count();

            for (var i = 0; i < partsCount; i++)
            {
                doneEvents[i] = new ManualResetEvent(false);

                resArrCalcs[i] = new ThreadPoolCalculater(arr, nextIndex, parts[i].Count(), doneEvents[i]);
                
                ThreadPool.QueueUserWorkItem(resArrCalcs[i].ThreadPoolCallback, i);
                
                nextIndex += parts[i].Count();
            }

            WaitHandle.WaitAll(doneEvents);

            foreach (var r in resArrCalcs)
                res += r.Result;

            return res;
        }
    }
}